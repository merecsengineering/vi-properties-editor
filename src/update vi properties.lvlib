﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="16008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">2780</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">12124142</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16745561</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*"!!!*Q(C=\&gt;1R4C-R&amp;-&lt;RD^57N,H"%MUC5&lt;_+%CH.(C"8?$V67MJQA"4P#LF#?C3EVWW&gt;+_1+MX_&lt;FY"71#B!1AA&lt;4W9_D_U@HN&amp;)P:R+0`8X7"F?,;V`\(59R`VB?$R\WD`M@R]0TY^`[$_-0QQ?`JPEJ@ZBX&amp;]/R`V(CL\,&gt;`H;Z7:]1`F9QIH5VBD(J!8.;;;W\*DE3:\E3:\E32\E12\E12\E1?\E4O\E4O\E4G\E2G\E2G\E2NYLO=B&amp;,H*)S?,*1MGES14*T6#5("*0YEE]C9&gt;,*:\%EXA34_,B&amp;C7?R*.Y%E`C9:A34_**0)EH]4"6FW3PZ(A3$^-L]!3?Q".Y!A^,+P!%A'#R9/*A%BA+/I/4Q".Y!A_H#DS"*`!%HM"$NQ*0Y!E]A3@Q-+4P3H2.K_2YG%;/R`%Y(M@D?*B;DM@R/"\(YXB94I\(]4A):U&amp;H=ABS"DEX/"?/R`(Q)]@D?"S0YX%]&gt;05HZ(VHGK:6=DS'R`!9(M.D?*B#BM@Q'"\$9XC96I&lt;(]"A?QW.Y7%K'R`!9(A.C,-LS-C9T"BIX'9(BY;_`,&gt;;@5H3*^3L6S[N[+65PG_IF5LU=KI?O?JCKB[4;@.7GKD:,N1GK@U[&amp;6G&amp;5C[A'NROVY\CF&lt;7BLWJ+WI-VJ-^K5.GF$X`H'X7[H\8;LT7;D^8KNZ8+JR7+B_8SOW7SG[83KS72S_!T]IBY_#!`@J2P/6Z&gt;XV[P@&amp;X^7F`@8NW=86\&gt;HZV=N``DPUG@Y.OK(HK\"(PU$C$J+O1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="support" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="AbbrevationClassesInLibrary.vi" Type="VI" URL="../_support/AbbrevationClassesInLibrary.vi"/>
		<Item Name="apply control changes.vi" Type="VI" URL="../_support/apply control changes.vi"/>
		<Item Name="change all descriptions.vi" Type="VI" URL="../_support/change all descriptions.vi"/>
		<Item Name="Get Copyright Path.vi" Type="VI" URL="../_support/Get Copyright Path.vi"/>
		<Item Name="GetCtrlData.vi" Type="VI" URL="../_support/GetCtrlData.vi"/>
		<Item Name="GetVIData.vi" Type="VI" URL="../_support/GetVIData.vi"/>
		<Item Name="GetVIDescription&amp;Help.vi" Type="VI" URL="../_support/GetVIDescription&amp;Help.vi"/>
		<Item Name="PrepandSignItemsNames.vi" Type="VI" URL="../_support/PrepandSignItemsNames.vi"/>
		<Item Name="SaveVI.vi" Type="VI" URL="../_support/SaveVI.vi"/>
		<Item Name="SetChanged.vi" Type="VI" URL="../_support/SetChanged.vi"/>
		<Item Name="SetControlsIndicators.vi" Type="VI" URL="../_support/SetControlsIndicators.vi"/>
		<Item Name="SetVIDescription&amp;Help.vi" Type="VI" URL="../_support/SetVIDescription&amp;Help.vi"/>
	</Item>
	<Item Name="Type Defs" Type="Folder">
		<Item Name="Cluster_Data.ctl" Type="VI" URL="../_support/Cluster_Data.ctl">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Event Data.ctl" Type="VI" URL="../_support/Event Data.ctl">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="user action.ctl" Type="VI" URL="../_support/user action.ctl">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
	</Item>
	<Item Name="Update VI Properties.vi" Type="VI" URL="../Update VI Properties.vi"/>
</Library>

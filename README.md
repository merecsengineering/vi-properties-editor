# README #

Fork of NI Properties Editor: https://forums.ni.com/t5/Developer-Center-Resources/VI-Properties-Editor/ta-p/3492371

## Release Notes ##
V1.3.0
- NEW: Add Copyright to all VIs
- BUG: Fixed: Copyright added always a new line

## Bitbucket Issues ##
Changed from anonymous to registered people, so that we can communicate with you.